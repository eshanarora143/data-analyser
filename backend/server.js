const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

require("dotenv").config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
var bodyParser = require("body-parser");

var jsonParser = bodyParser.json({
  limit: 1024 * 1024 * 20,
  type: "application/json"
});
var urlencodedParser = bodyParser.urlencoded({
  extended: true,
  limit: 1024 * 1024 * 20,
  type: "application/x-www-form-urlencoding"
});

app.use(jsonParser);
app.use(urlencodedParser);

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true });
const connection = mongoose.connection;
connection.once("open", () => {
  console.log("MongoDB database connection established successfully");
});

const tableRouter = require("./routes/tables");
const table2Router = require("./routes/tables2");
//
app.use("/tables", tableRouter);
app.use("/tables2", table2Router);

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
