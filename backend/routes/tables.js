const router = require("express").Router();
let Table = require("../models/table");
let Table2 = require("../models/table2");

router.route("/add").post((req, res) => {
  var arr = req.body.data;
  Table.create(arr, function(err, array) {
    if (err) {
      console.log(err);
      // terminate request/response cycle
      return res.send("Error saving");
    }
    res.json(" added!");
  });
});

router.route("/join").post((req, res) => {
  Table2.aggregate([
    {
      $lookup: {
        from: "tables",
        localField: "CustomerID",
        foreignField: "CustomerID",
        as: "order"
      }
    }
  ])

    .then(response => {
      res.status(200).json({ data: response });
    })
    .catch(err => {
      res.status(400).json({ data: err });
    });
});
router.route("/sort").post((req, res) => {
  Table2.aggregate([
    {
      $lookup: {
        from: "tables",
        localField: "CustomerID",
        foreignField: "CustomerID",
        as: "order"
      }
    }
  ])

    .then(response => {
      res.status(200).json({ data: response });
    })
    .catch(err => {
      res.status(400).json({ data: err });
    });
});

module.exports = router;
