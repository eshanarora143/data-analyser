const router = require("express").Router();
let Table2 = require("../models/table2");

router.route("/add").post((req, res) => {
  var arr = req.body.data;
  Table2.create(arr, function(err, array) {
    if (err) {
      console.log(err);
      return res.send("Error saving");
    }
    res.json(" added!");
  });
});

module.exports = router;
