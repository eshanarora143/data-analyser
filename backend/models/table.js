const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const tableSchema = new Schema({
  CustomerID: {
    type: String
  },
  CompanyName: {
    type: String
  },
  ContactName: {
    type: String
  },
  ContactTitle: {
    type: String
  }
});

const Table = mongoose.model("Table", tableSchema);

module.exports = Table;
