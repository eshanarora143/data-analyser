const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const table2Schema = new Schema({
  CustomerID: {
    type: String
  },
  OrderId: {
    type: String
  },
  EmployeeID: {
    type: String
  },
  OrderDate: {
    type: String
  },
  RequiredDate: {
    type: String
  },
  ShippedDate: {
    type: String
  },
  ShipVia: {
    type: String
  },
  Freight: {
    type: String
  }
});

const Table2 = mongoose.model("Table2", table2Schema);

module.exports = Table2;
