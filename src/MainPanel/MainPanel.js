import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import { withStyles, Paper } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Checkbox from "@material-ui/core/Checkbox";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import Popover from "@material-ui/core/Popover";
import PopupState, { bindTrigger, bindPopover } from "material-ui-popup-state";

const columns = [
  "CustomerID",
  "EmployeeID",
  "Freight",
  "OrderDate",
  "RequiredDate",
  "ShipVia",
  "ShippedDate"
];

const TabPanel = props => {
  const { value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    ></Typography>
  );
};

class MainPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      database: "nosql",
      csvfile: undefined,
      data: null,
      value: 1,
      selectedFile: [],
      loaded: null,
      arr: [],
      file: null
    };
  }
  ondragenter = e => {
    e.stopPropagation();
    e.preventDefault();
  };

  dragOver = e => {
    e.preventDefault();
    console.log("uesh");
  };
  onDrag = (e, id) => {
    console.log("drag start");
    console.log(id);
    e.dataTransfer.setData("table_id", id);
  };

  handleChange = database => {
    this.setState({ database: database }, () => {
      this.state.database == "nosql"
        ? this.setState({ value: 1 })
        : this.setState({ value: 0 });
    });
  };
  handleTabChange = (event, newValue) => {
    this.setState({ value: newValue });
  };

  onDrop = (e, key) => {
    // POST to a test endpoint for demo purposes
    // const req = request.post('https://httpbin.org/post');
    e.preventDefault();
    console.log(key);

    const dt = e.dataTransfer.getData("table_id");
    var Papa = require("papaparse/papaparse.min.js");
    key == 0
      ? Papa.parse(this.state.selectedFile[key], {
          complete: this.updateData,
          header: true
        })
      : Papa.parse(this.state.selectedFile[key], {
          complete: this.updateData2,
          header: true
        });

    console.log(dt);
  };
  updateData = result => {
    var data = result.data;
    console.log(data);
    axios
      .post("http://localhost:5000/tables/add", { data })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log("unable to save", err);
      });
  };
  updateData2 = result => {
    var data = result.data;
    console.log(data);
    axios
      .post("http://localhost:5000/tables2/add", { data })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log("unable to save", err);
      });
  };
  onJoin = () => {
    axios
      .post("http://localhost:5000/tables/join")
      .then(res => {
        console.log(res, "res");
        this.setState({ data: res.data.data }, () => {
          console.log(this.state.data);
        });
      })
      .catch(err => {
        console.log("unable to save", err);
      });
  };
  onSort = () => {
    axios
      .post("http://localhost:5000/tables/sort")
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log("unable to save", err);
      });
  };

  onChangeFile = event => {
    this.setState(
      {
        selectedFile: [...this.state.selectedFile, event.target.files[0]],
        loaded: 0
      },
      () => {
        console.log(this.state.selectedFile);
        localStorage.setItem(
          this.state.selectedFile.name,
          this.state.selectedFile
        );
      }
    );
  };

  render() {
    const classes = this.props.classes;

    return (
      <div>
        <AppBar
          position="fixed"
          style={{
            backgroundColor: "#39014A",
            padding: 5,
            borderRadius: 5,
            boxShadow: "none",
            border: "solid",
            borderColor: "#39014A"
          }}
        >
          <Toolbar>
            <h1 style={{ color: "white" }}>Data Analyser</h1>
          </Toolbar>
        </AppBar>
        <Grid container style={{ marginTop: 100 }}>
          <Grid xs={6} container>
            <Grid xs={5}>
              <ExpansionPanel>
                <div id="panel1a-header">
                  <Checkbox
                    checked={this.state.checkedA}
                    onChange={() => this.handleChange("nosql")}
                    value="my SQL"
                    color="primary"
                    inputProps={{
                      "aria-label": "secondary checkbox"
                    }}
                  />
                  <Typography style={{ marginTop: "10px" }}>my SQL</Typography>
                </div>
                <ExpansionPanelDetails style={{ display: this.state.display }}>
                  <Grid container>
                    <Grid xs={12}>
                      <TextField
                        id="standard-name"
                        label="Username"
                        margin="normal"
                      />
                    </Grid>
                    <Grid xs={12}>
                      <TextField
                        id="standard-name"
                        label="Password"
                        margin="normal"
                        type="password"
                      />
                    </Grid>
                    <Grid xs={12}>
                      <TextField
                        id="standard-name"
                        label="IP Address"
                        margin="normal"
                      />
                    </Grid>
                    <Grid xs={12}>
                      <Grid xs={12} style={{ justifyContent: "center" }}>
                        <Button
                          variant="contained"
                          color="primary"
                          className={classes.button}
                        >
                          Submit
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </ExpansionPanelDetails>
              </ExpansionPanel>
              <ExpansionPanel>
                <div id="panel2a-header">
                  <Checkbox
                    checked={this.state.checkedB}
                    onChange={() => this.handleChange("sql")}
                    value="csv"
                    color="primary"
                    inputProps={{
                      "aria-label": "secondary checkbox"
                    }}
                  />
                  <Typography style={{ marginTop: "10px" }}>CSV</Typography>
                </div>
                <ExpansionPanelDetails style={{ display: "flex" }}>
                  <input type="file" name="file" onChange={this.onChangeFile} />
                  {/* <button type="button" class="btn btn-success btn-block" onClick={this.onClickHandler}>Upload</button>  */}
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </Grid>
            <Grid xs={5} style={{ marginLeft: 10 }}>
              <Paper>
                <Tabs
                  value={this.state.value}
                  indicatorColor="primary"
                  textColor="primary"
                  onChange={this.handleTabChange}
                  aria-label="disabled tabs example"
                >
                  <Tab label="CSV" />
                  <Tab label="my SQL" />
                </Tabs>
                <TabPanel value={this.state.value} index={0}>
                  {this.state.selectedFile
                    ? this.state.selectedFile.map((file, index) => (
                        <li draggable onDragStart={e => this.onDrag(e, file)}>
                          {file.name}
                        </li>
                      ))
                    : ""}
                </TabPanel>
                <TabPanel value={this.state.value} index={1}>
                  <ExpansionPanel>
                    <ExpansionPanelSummary
                      // expandIcon={<ExpandMoreIcon />}
                      // aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      {this.state.selectedFile ? (
                        <Typography style={{ marginTop: "10px" }}>
                          {this.state.selectedFile.name}
                        </Typography>
                      ) : (
                        ""
                      )}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails
                      style={{ display: this.state.display }}
                    ></ExpansionPanelDetails>
                  </ExpansionPanel>
                  <ExpansionPanel>
                    <ExpansionPanelSummary
                      // expandIcon={<ExpandMoreIcon />}
                      // aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      {this.state.selectedFile ? (
                        <Typography style={{ marginTop: "10px" }}>
                          {this.state.selectedFile.name}
                        </Typography>
                      ) : (
                        ""
                      )}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails
                      style={{ display: this.state.display }}
                    ></ExpansionPanelDetails>
                  </ExpansionPanel>
                  <ExpansionPanel>
                    <ExpansionPanelSummary
                      // expandIcon={<ExpandMoreIcon />}
                      // aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      {this.state.selectedFile ? (
                        <Typography style={{ marginTop: "10px" }}>
                          {this.state.selectedFile.name}
                        </Typography>
                      ) : (
                        ""
                      )}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails
                      style={{ display: this.state.display }}
                    ></ExpansionPanelDetails>
                  </ExpansionPanel>
                </TabPanel>
              </Paper>
            </Grid>
          </Grid>
          <Grid xs={6} container>
            <Paper
              style={{
                backgroundColor: "#f5f5f5",
                width: "600px",
                height: "500px"
              }}
            >
              <Grid container>
                <Grid xs={4} container>
                  <Grid xs={4} style={{ marginLeft: 50, marginTop: 50 }}>
                    <Paper
                      style={{
                        backgroundColor: "grey",
                        color: "white",
                        width: 100,
                        padding: 5
                      }}
                    >
                      <div
                        onDragOver={this.dragOver}
                        onDrop={e => this.onDrop(e, "0")}
                      >
                        Drop Table 1
                      </div>
                    </Paper>
                  </Grid>
                  <Grid xs={4} style={{ marginLeft: 50, marginTop: 50 }}>
                    <Paper
                      style={{
                        backgroundColor: "grey",
                        color: "white",
                        width: 100,
                        padding: 5
                      }}
                    >
                      <div
                        key="1"
                        onDragOver={this.dragOver}
                        onDrop={e => this.onDrop(e, "1")}
                      >
                        Drop Table2
                      </div>
                    </Paper>
                  </Grid>
                </Grid>
                <Grid xs={4} container>
                  <Grid xs={4} style={{ marginLeft: 50, marginTop: 80 }}>
                    {/* <Dropzone onDrop={this.onDrop} /> */}
                    <PopupState variant="popover" popupId="demo-popup-popover">
                      {popupState => (
                        <div>
                          <Button
                            variant="contained"
                            {...bindTrigger(popupState)}
                          >
                            Join
                          </Button>
                          <Popover
                            {...bindPopover(popupState)}
                            anchorOrigin={{
                              vertical: "bottom",
                              horizontal: "center"
                            }}
                            transformOrigin={{
                              vertical: "top",
                              horizontal: "center"
                            }}
                          >
                            <Paper
                              style={{
                                backgroundColor: "grey",
                                color: "white",
                                minWidth: 100,
                                minHeight: 100
                              }}
                            >
                              <h4>Please select the join</h4>
                              <select name="cars">
                                <option value="innerJoin">inner join</option>
                                <option value="">###</option>
                                <option value="">###</option>
                                <option value="">###</option>
                              </select>
                            </Paper>
                          </Popover>
                        </div>
                      )}
                    </PopupState>{" "}
                  </Grid>
                  <Grid xs={4} style={{ marginLeft: 50, marginTop: 150 }}>
                    <Paper
                      onClick={this.onSort}
                      style={{
                        backgroundColor: "grey",
                        color: "white",
                        width: 100,
                        height: 20
                      }}
                    >
                      Transform
                    </Paper>
                  </Grid>
                  <Grid xs={4} style={{ marginLeft: 50, marginTop: 150 }}>
                    <Paper
                      onClick={this.onJoin}
                      style={{
                        backgroundColor: "#2196f3",
                        color: "white",
                        width: 100
                      }}
                    >
                      Run Mapping
                    </Paper>
                  </Grid>
                </Grid>
                <Grid xs={4}></Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
        {this.state.data ? (
          <MUIDataTable
            className={classes.root}
            title="Preview of the output"
            data={this.state.data}
            columns={columns}
          />
        ) : (
          ""
        )}
      </div>
    );
  }
}

const styles = theme => ({
  paper: {
    paddingTop: 100,
    minHeight: "100vh",
    backgroundColor: "#080037"
  }
});
export default withStyles(styles)(MainPanel);
